#!/bin/bash
apt full-upgrade -y
apt install zsh zsh-autosuggestions -y
apt install htop screenfetch ranger fzf tree -y
apt install virtualbox docker.io -y
git clone https://gitee.com/mirrors/oh-my-zsh.git /home/hwj/.oh-my-zsh
cp /home/hwj/Documents/dot/weijie-wedisagree.zsh-theme /home/hwj/.oh-my-zsh/custom/themes
cp /home/hwj/Documents/dot/zshrc /home/hwj/.zshrc
cp /home/hwj/Documents/dot/vimrc /home/hwj/.vimrc
