# Ubuntu 配置

## 0. 国内阿里云ISO镜像下载地址
```
https://mirrors.aliyun.com/ubuntu-releases/
```

## 1. 更换`/etc/apt/sources.list`文件里的镜像源

1. 首先备份源列表

```
sudo cp /etc/apt/sources.list /etc/apt/sources.list_backup
```

2. 打开`sources.list` 文件修改镜像源

```
sudo vi /etc/apt/sources.list
```

镜像源
```
# ubuntu 16.04 配置如下
deb http://mirrors.aliyun.com/ubuntu/ xenial main
deb-src http://mirrors.aliyun.com/ubuntu/ xenial main

deb http://mirrors.aliyun.com/ubuntu/ xenial-updates main
deb-src http://mirrors.aliyun.com/ubuntu/ xenial-updates main

deb http://mirrors.aliyun.com/ubuntu/ xenial universe
deb-src http://mirrors.aliyun.com/ubuntu/ xenial universe
deb http://mirrors.aliyun.com/ubuntu/ xenial-updates universe
deb-src http://mirrors.aliyun.com/ubuntu/ xenial-updates universe

deb http://mirrors.aliyun.com/ubuntu/ xenial-security main
deb-src http://mirrors.aliyun.com/ubuntu/ xenial-security main
deb http://mirrors.aliyun.com/ubuntu/ xenial-security universe
deb-src http://mirrors.aliyun.com/ubuntu/ xenial-security universe


# ubuntu 18.04(bionic) 配置如下
deb http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse


# ubuntu 20.04(focal) 配置如下
deb http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse

deb http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
```

## 3. 刷新列表

```
sudo apt update
sudo apt full-upgrade
# sudo apt install build-essential
```

## 4. 开放22端口

4.1 查看端口开放状态

打开防火墙
```
sudo ufw enable
```

```
sudo ufw status
```

4.2 开放指定端口

```
sudo ufw allow 22
```

## 5. 设置静态IP

如果是18.04以后的版本:
修改 `/etc/netplan/01-network-manager-all.yaml` 文件
> 注意: 01-network-manager-all.yaml 文件名不一定都一样,根据自己的具体情况决定

```
# 默认应该是这样的:

# Let NetworkManager manage all devices on this system
network:
  version: 2
  renderer: NetworkManager
```

修改成这样:

```
# Let NetworkManager manage all devices on this system
network:
  version: 2
  renderer: networkd
  ethernets:
    wlp2s0:
      dhcp4: no
      addresses: [192.168.31.210/24]
      gateway4: 192.168.31.1
      nameservers:
        addresses: [119.29.29.29]
      dhcp6: no
```

应用新配置

```sh
sudo netplan apply
```

> 注意: `NetworkManager` 好像是用于图形界面下设置, `networkd` 好像是用于无图形界面下设置. 
但是试了下发现如果设置成 `NetworkManager` 最后的固定IP没有生效. 但是设置成 `networkd` 最后又会有2个IP地址.
不过这样起码是有一个固定的IP地址了.
`wlp2s0`是你要设置固定IP的网卡名字,可以通过`ip addr`查看.

## 6. 重置root账户密码

ubuntu 不推荐设置root用户密码,所以装完系统后的密码是随机的.
可以通过`sudo passwd`修改它.
此时会要求先输入当前用户的密码,然后设置root用户的密码(需要输入2次确认).

## 7. 配置终端代理

```
sudo apt install privoxy
```

安装完成会提示:
```
Creating config file /etc/privoxy/config with new version
Created symlink /etc/systemd/system/multi-user.target.wants/privoxy.service → /lib/systemd/system/privoxy.service.
```

然后修改它的配置文件 /etc/privoxy/config，在文件末尾添加如下内容：

```
forward-socks5 / 127.0.0.1:1080 . # SOCKS5代理地址
listen-address 127.0.0.1:8080     # HTTP代理地址
forward 10.*.*.*/ .               # 内网地址不走代理
forward .abc.com/ .             # 指定域名不走代理
```

其中，第1行的 127.0.0.1:1080 是你在本地的sock5代理地址，而第二行的 127.0.0.1:8080 则是sock5转换成的 http 代理地址，最后两行指定了两个不走代理的地址。

配置好之后重启Privoxy服务：

```
sudo /etc/init.d/privoxy restart
```

然后在命令行设置http_proxy和https_proxy两个环境变量：

```
export http_proxy="127.0.0.1:8080"
export https_proxy="127.0.0.1:8080"
```

## 查看gcc版本

```
gcc -v
```


# 参考链接
[【Ubuntu】Ubuntu 18.04 LTS 更换国内源——解决终端下载速度慢的问题 - 知乎](https://zhuanlan.zhihu.com/p/61228593)

[Ubuntu系统中防火墙的使用和开放端口_Aaron_Run的博客-CSDN博客_ubuntu 防火墙开放端口](https://blog.csdn.net/qq_36938617/article/details/95234909)

[安装Ubuntu版本linux过程中没有提示设置root用户密码问题的解决办法_Dragon_F的专栏-CSDN博客_kali linux在系统安装过程中，没有设置root用户口令的过程？如何修改root口令？](https://blog.csdn.net/helongfu/article/details/45012759)

[如何在Ubuntu 18.04上配置静态IP地址](https://www.myfreax.com/how-to-configure-static-ip-address-on-ubuntu-18-04/?page=4)

[如何在Ubuntu 20.04 / 18.04 / 19.10中配置静态IP地址 | 代码日志](https://zh.codepre.com/ubuntu-4234.html)

[ubuntu – 使用NetworkManager / nmcli删除辅助IP - CocoaChina_一站式开发者成长社区](http://www.cocoachina.com/articles/404184)

[关于 Ubuntu 20.04 LTS 的网络（dchp/dns/route/PPPoE） - qbit snap - SegmentFault 思否](https://segmentfault.com/a/1190000022751690)

[写给工程师的 Ubuntu 20.04 最佳配置指南 - 知乎](https://zhuanlan.zhihu.com/p/139305626)
