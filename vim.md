## 将当前目录下的vimrc文件拷贝到用户目录下

```
cp ./vimrc ~/.vimrc
```

重新进入vim界面就会自动加载该配置文件

如果报错的话,根据报错提示去修改.

1. 如果不支持部分语法,那么可能是vim版本过低

```
sudo apt install vim -y
```

2. 如果找不到定义的主题,可能也是vim版本过低

可以到`/usr/share/vim/vim81/colors`目录下确认存在的vim自带的主题文件,以`.vom`结尾.
