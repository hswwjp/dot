#!/bin/bash
#sudo mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
#sudo curl -o /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-8.repo
dnf upgrade -y
dnf install zsh zsh-autosuggestions ranger fzf -y
cp ~/dot/zshrc ~/.zshrc &&\
    git clone https://gitee.com/mirrors/oh-my-zsh.git ~/.oh-my-zsh &&\
    cp ~/dot/weijie-wedisagree.zsh-theme ~/.oh-my-zsh/custom/themes &&\
    sed -i "s/MacMini/DockerContainer/g" ~/.oh-my-zsh/custom/themes/weijie-wedisagree.zsh-theme &&\
