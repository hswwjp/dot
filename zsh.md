# zsh 配置

## 1. 安装zsh

```
sudo apt install zsh
```

## 2. 安装oh-my-zsh

```
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

2.1 卸载oh-my-zsh

```
uninstall_oh_my_zsh
```

## 3. 国内环境安装oh-my-zsh

```sh
git clone https://gitee.com/mirrors/oh-my-zsh.git ~/.oh-my-zsh
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
```

或者

将当前目录下的oh-my-zsh.gz文件解压到用户目录下,并执行安装脚本

```
tar -zxvf oh-my-zsh.gz ~/.on-my-zsh
cd ~/.on-my-zsh/tools
sh ./install.sh
```

> 注意: 该install.sh脚本经过魔改,删除了联网下载on-my-zsh的部分代码

## 3. 改变默认 shell为 zsh

chsh -s /bin/zsh

## 4. 查看zsh版本

```
zsh --version
```

## 5. 下载自动补全插件

```
git clone git://github.com/zsh-users/zsh-autosuggestions $ZSH/custom/plugins/zsh-autosuggestions
```

5.1 在 `~/.zshrc` 文件中添加插件名

```
plugins=(git zsh-autosuggestions vi-mode)
```

5.2 配置快捷键(Ctrl+f)

```
bindkey '^F' autosuggest-accept
```

> 注意: 绑定快捷键需放在加载插件后面才能生效

## 6. 自定义主题

将目录下的weijie-wedisagree.zsh-theme主题放到themes目录下

```
cp ./weijie-wedisagree.zsh-theme $ZSH/custom/themes
```

## 7. 刷新配置文件

```
source ~/.zshrc
```
