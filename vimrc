" 不兼容vi模式
set nocp
" set nocompatible
" 开启代码高亮
syntax on
" 修改主题
colo desert
" colo skeletor
" 显示行号
" set nu
set number
" 显示相对行号
set relativenumber
" 鼠标所在行显示下划线
set cursorline
" 字不会超出当前屏幕,换行
set wrap
" 显示命令
set showcmd
"显示括号配对情况
set showmatch
" 解决插入模式下delete/backspce键失效问题
" set backspace=2
"开启normal 或visual模式下的backspace键空格键，左右方向键,insert或replace模式下的左方向键，右方向键的跳行功能(还不知道什么作用?)
" set whichwrap=b,s,<,>,[,] 
" 缺省情况下，h和l命令不会把光标移出当前行。如果已经到了行首，无论按多少次h键，光标始终停留在行首，l命令也类似。如果希望h和l命令可以移出当前行，更改'whichwrap'选项的设置
" set whichwrap+=<,>,h,l
" 普通模式输入命令按 Tab 键会有提示
set wildmenu
" 允许鼠标
set mouse=a
" 搜索结果高亮
set hlsearch
" set hls
" 运行 nohlsearch 命令
" exec "nohlsearch"
" 支持增量搜索
" include search
set incsearch
" 搜索结果忽略大小写
set ignorecase
" 智能大小写
set smartcase
" 自动缩进
set autoindent 
" 智能缩进
set smartindent 
" 解决自动换行格式下, 如高度在折行之后超过窗口高度结果这一行看不到的问题
" set display=lastline
" 光标移动到buffer的顶部和底部时保持3行距离
set scrolloff=3
" 把所有数字当成十进制
set nrformats=
" 换行时自动缩进列数为4列
set shiftwidth=4
" 插入模式下,一次tab按键,展示4个空格,而不是\t
set softtabstop=4
" shiftwidth 引入的缩进，是 Tab 还是 Space?
" 如果 expandtab 开启是 Space
" noexpandtab 且 softtabstop 与 tabstop 一致时，是 TAB 符号。
set expandtab
" 将 Vim 默认的无名寄存器设置成系统剪贴板
" set clipboard=unnamed 
" set pastetoggle=<F2> " 按F2 进入粘贴模式
" set foldmethod=indent " 设置折叠方式
set encoding=utf-8 " 新创建文件格式为utf-8
set termencoding=utf-8 " 终端显示格式，把解析的字符用utf-8编码来进行显示和渲染终端屏幕
set fileencodings=utf-8,gb18030,gbk,cp936,gb2312 " 可以查看多种格式的文件

"set statusline
" 设置状态行显示常用信息
" %F 完整文件路径名
" %m 当前缓冲被修改标记
" %m 当前缓冲只读标记
" %h 帮助缓冲标记
" %w 预览缓冲标记
" %Y 文件类型
" %b ASCII值
" %B 十六进制值
" %l 行数
" %v 列数
" %p 当前行数占总行数的的百分比
" %L 总行数
" %{...} 评估表达式的值，并用值代替
" %{"[fenc=".(&fenc==""?&enc:&fenc).((exists("+bomb") && &bomb)?"+":"")."]"} 显示文件编码
" %{&ff} 显示文件类型
set statusline=%F%m%r%h%w%=\ [ft=%Y]\ %{\"[fenc=\".(&fenc==\"\"?&enc:&fenc).((exists(\"+bomb\")\ &&\ &bomb)?\"+\":\"\").\"]\"}\ [ff=%{&ff}]\ [asc=%03.3b]\ [hex=%02.2B]\ [pos=%04l,%04v][%p%%]\ [len=%L]
" set ruler                   " 打开状态栏标尺
" set ru                   " 打开状态栏标尺

" 设置 laststatus = 0 ，不显式状态行
" 设置 laststatus = 1 ，仅当窗口多于一个时，显示状态行
" 设置 laststatus = 2 ，总是显式状态行
set laststatus=2

" 开启真彩色 
if has("termguicolors")
    " fix bug for vim
    set t_8f=[38;2;%lu;%lu;%lum
    set t_8b=[48;2;%lu;%lu;%lum

    " enable true color
    set termguicolors
endif

" 将 leader 键设成空格键
let mapleader=" "
" let g:mapleader=","

" 普通/视图/运算符模式映射
" map S :w<CR>
map Q :q<CR>
" map R :source ~/.vimrc<CR>
map J 5j
map K 5k
map <space> viw

" 左右分屏,光标在右屏
" map sl :set splitright<CR>:vsplit<CR>
" 左右分屏,光标在左屏
" map sh :set nosplitright<CR>:vsplit<CR>
" 上下分屏,光标在上屏
" map sk :set nosplitbelow<CR>:split<CR>
" 上下分屏,光标在下屏
" map sj :set splitbelow<CR>:split<CR>
" 分屏模式下,空格+k 映射成移动到上屏
map <C-k> <C-w>k
" 分屏模式下,空格+j 映射成移动到下屏
map <C-j> <C-w>j
" 分屏模式下,空格+h 映射成移动到左屏
map <C-h> <C-w>h
" 分屏模式下,空格+l 映射成移动到右屏
map <C-l> <C-w>l
" 调整分屏大小
map <up> :res +5<CR>
map <down> :res -5<CR>
map <left> :vertical resize-5<CR>
map <right> :vertical resize+5<CR>

" 新建标签页
" map tt :tabe<CR>
" 切换到上一个标签页
" map th :-tabnext<CR>
" 切换到下一个标签页
" map tl :+tabnext<CR>

map #2 :set relativenumber
map #3 :set norelativenumber

" 将普通模式的空格+回车映射成取消搜索高亮
noremap <LEADER><CR> :nohlsearch<CR>

" 将 = 映射成 向下移动一行并保持当前行在屏幕中间
noremap = jzz
noremap - kzz
" 切换 buffer
" nnoremap <silent> [b :bprevious<CR>
" nnoremap <silent> [n :bnext<CR>

inoremap <c-d> <Esc>ddi
" `^ 表示最后一次编辑的光标位置, 可以输入:h `^查看具体的帮助
" inoremap jj <Esc>`^

" json 格式化
com! FormatJSON %!python3 -m json.tool

"Mode Settings
let &t_SI.="\e[5 q" "SI = INSERT mode
let &t_SR.="\e[4 q" "SR = REPLACE mode
let &t_EI.="\e[1 q" "EI = NORMAL mode (ELSE)
"Cursor settings:
"  1 -> blinking block
"  2 -> solid block
"  3 -> blinking underscore
"  4 -> solid underscore
"  5 -> blinking vertical bar
"  6 -> solid vertical bar
"let &t_SI = "\<Esc>]50;CursorShape=1\x7"
"let &t_SR = "\<Esc>]50;CursorShape=2\x7"
"let &t_EI = "\<Esc>]50;CursorShape=0\x7"






" 纯手工制作一个漂亮的 statusline
"function! Buf_total_num()
    "return len(filter(range(1, bufnr('$')), 'buflisted(v:val)'))
"endfunction
"function! File_size(f)
    "let l:size = getfsize(expand(a:f))
    "if l:size == 0 || l:size == -1 || l:size == -2
        "return ''
    "endif
    "if l:size < 1024
        "return l:size.' bytes'
    "elseif l:size < 1024*1024
        "return printf('%.1f', l:size/1024.0).'k'
    "elseif l:size < 1024*1024*1024
        "return printf('%.1f', l:size/1024.0/1024.0) . 'm'
    "else
        "return printf('%.1f', l:size/1024.0/1024.0/1024.0) . 'g'
    "endif
"endfunction
"set statusline=%<%1*[B-%n]%*
"" TOT is an abbreviation for total
"set statusline+=%2*[TOT:%{Buf_total_num()}]%*
"set statusline+=%3*\ %{File_size(@%)}\ %*
"set statusline+=%4*\ %F\ %*
"set statusline+=%5*『\ %{exists('g:loaded_ale')?ALEGetStatusLine():''}』%{exists('g:loaded_fugitive')?fugitive#statusline():''}%*
"set statusline+=%6*\ %m%r%y\ %*
"set statusline+=%=%7*\ %{&ff}\ \|\ %{\"\".(&fenc==\"\"?&enc:&fenc).((exists(\"+bomb\")\ &&\ &bomb)?\",B\":\"\").\"\ \|\"}\ %-14.(%l:%c%V%)%*
"set statusline+=%8*\ %P\ %*
"" default bg for statusline is 236 in space-vim-dark
"hi User1 cterm=bold ctermfg=232 ctermbg=179
"hi User2 cterm=None ctermfg=214 ctermbg=242
"hi User3 cterm=None ctermfg=251 ctermbg=240
"hi User4 cterm=bold ctermfg=169 ctermbg=239
"hi User5 cterm=None ctermfg=208 ctermbg=238
"hi User6 cterm=None ctermfg=246 ctermbg=237
"hi User7 cterm=None ctermfg=250 ctermbg=238
"hi User8 cterm=None ctermfg=249 ctermbg=240
" end纯手工制作一个漂亮的 statusline
