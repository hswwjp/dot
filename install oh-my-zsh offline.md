# 1. 获取 Oh My Zsh 仓库代码

Oh My Zsh 的仓库代码托管在 GitHub 上，地址为：
GitHub: [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)

不过有些人可能从 GitHub 上下载代码很慢，没关系，我还有码云上的该项目的镜像仓库：
码云: [oh-my-zsh](https://gitee.com/mirrors/oh-my-zsh)


# 2. 放置 oh-my-zsh 到 $HOME 下，重命名为 .oh-my-zsh
根据我在联网环境下的经验，oh-my-zsh 安装完之后，其存在于 $HOME 目录下，并且以 .oh-my-zsh 的文件夹名称为名（下面的截图是已经配置好了 Oh My Zsh 的环境截图，只是为了方便大家查看 .oh-my-zsh 的位置）：

![](https://img-blog.csdnimg.cn/20190909183813118.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTI4MTQ4NTY=,size_16,color_FFFFFF,t_70)

因此，我们需要将我们在第 1 步中的仓库代码解压后放到 $HOME 目录下，然后命名为 .oh-my-zsh：

```js
$ cd
$ mv oh-my-zsh .oh-my-zsh
```

# 3. 修改 $HOME/.oh-my-zsh/tools 下的 install.sh 安装脚本

接下来就是重头戏，我们要对 Oh My Zsh 的安装脚本进行魔改，将其联网下载仓库的代码全部删掉。这里，我建议将 install.sh 备份下再进行修改：

```
$ cp install.sh install.sh.back
```

修改步骤如下：

a. 删除 setup_ohmyzsh 函数

```ruby
setup_ohmyzsh() {
	# Prevent the cloned repository from having insecure permissions. Failing to do
	# so causes compinit() calls to fail with "command not found: compdef" errors
	# for users with insecure umasks (e.g., "002", allowing group writability). Note
	# that this will be ignored under Cygwin by default, as Windows ACLs take
	# precedence over umasks except for filesystems mounted with option "noacl".
	umask g-w,o-w

	echo "${BLUE}Cloning Oh My Zsh...${RESET}"

	command_exists git || {
		error "git is not installed"
		exit 1
	}

	if [ "$OSTYPE" = cygwin ] && git --version | grep -q msysgit; then
		error "Windows/MSYS Git is not supported on Cygwin"
		error "Make sure the Cygwin git package is installed and is first on the \$PATH"
		exit 1
	fi

	git clone --depth=1 --branch "$BRANCH" "$REMOTE" "$ZSH" || {
		error "git clone of oh-my-zsh repo failed"
		exit 1
	}

	echo
}
```

![](https://img-blog.csdnimg.cn/2019090918221042.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTI4MTQ4NTY=,size_16,color_FFFFFF,t_70)

b. 搜索 setup_ohmyzsh 字符串，将调用的地方删除

c. 搜索字符串 You already have Oh My Zsh installed.，并将附近 if 语句全部删除

```ruby
	if [ -d "$ZSH" ]; then
		cat <<-EOF
			${YELLOW}You already have Oh My Zsh installed.${RESET}
			You'll need to remove '$ZSH' if you want to reinstall.
		EOF
		exit 1
	fi

	setup_ohmyzsh
```

![](https://img-blog.csdnimg.cn/20190909183730359.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTI4MTQ4NTY=,size_16,color_FFFFFF,t_70)

# 4. 执行安装脚本 install.sh
接下来执行安装脚本 install.sh 文件：

```
$ sh install.sh
```

如果报错，接着打开 install.sh 修改代码问题，如果没有报错的话，就会进入 Oh My Zsh 的安装界面，再然后就会进入 Oh My Zsh 的默认主题：

![](https://img-blog.csdnimg.cn/20190909183654323.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTI4MTQ4NTY=,size_16,color_FFFFFF,t_70)

至此，我们终于离线安装上了 Oh My Zsh 啦！

# 参考链接

[与 Oh My Zsh 不可错过的邂逅：如何离线安装 Oh My Zsh_(ÒωÓױ)-CSDN博客_离线安装oh my zsh](https://blog.csdn.net/u012814856/article/details/100668640)
