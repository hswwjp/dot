# 1. 安装命令行Markdown文件查看

```
sudo gem install mdless
```

> 注意:如果没有`gem`命令,可能是没有`ruby`环境.

安装`ruby`

```
sudo apt install ruby -y
```

查看`ruby`版本

```
ruby -v
```

## `mdless`的使用

```
mdless filename.md
```
