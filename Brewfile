tap "homebrew/bundle"
tap "homebrew/cask"
tap "homebrew/core"
tap "homebrew/services"
tap "caskroom/cask"
# java
tap "caskroom/versions"
tap "adoptopenjdk/openjdk"
tap "homebrew/cask-versions"
tap "homebrew/cask-fonts"
tap "caskroom/cask-fonts"
tap "jesseduffield/lazydocker"


### 常用软件
cask "iterm2"
cask "docker"
cask "alfred"
cask "sublime-text"
cask "google-chrome"
cask "iina"
cask "visual-studio-code"
cask "postman"
cask "mysqlworkbench"
cask "appcleaner"
cask "wechat"
cask "qq"
cask "wpsoffice"
brew "denisidoro/tools/navi"
cask "anki"
cask "another-redis-desktop-manager"

# 背景音乐音量管理
cask "background-music"

# 网易云音乐
cask "neteasemusic"

# 印象笔记
cask "yinxiangbiji"

# 有道云词典
cask "youdaodict"

# 有道云笔记
cask "youdaonote"

# QQ音乐
cask "qqmusic"

# 百度云网盘
cask "baiducloud"

# 思维导图软件
cask "mindmaster"

# 数据库管理工具，支持多种数据库
cask "tableplus"

# 截图工具
cask "snipaste"

# 阿里钉钉
cask "dingtalk"

# Cakebrew - Homebrew 的客户端软件。摆脱命令方便安装、查看、卸载软件
cask "cakebrew"

# HTTPie: a CLI, cURL-like tool for humans(比curl更好用的访问URL工具)
brew install httpie

# 迅雷
cask "thunder"

# Markdown编辑器
cask "typora"

# 优酷
cask "youku"

# 腾讯视频
cask "qqlive"

# 爱奇艺
cask "qiyimedia"

# 隐藏系统菜单栏
cask "dozer"

# 外接屏幕亮度状态
cask "lunar"

# 日历
cask "itsycal"


### 不常用软件
brew "gradle"
brew "groovy"
cask "free-download-manager"
cask "notable"
cask "pycharm-ce"
cask "spotify"
cask "tencent-lemon"
cask "virtualbox"
cask "flux"
cask "paper"
cask "xmind-zen"
#cask "firefox"
#cask "aliwangwang"
#cask "android-studio"
#cask "caffeine"
# Spotify 和 iTunes 在状态菜单栏中显示
#cask "spotmenu"
brew "p7zip"
brew "screenfetch"
brew "sshfs"




### 这些插件为Mac Quick Look添加了对相应文件类型的支持
cask "qlcolorcode"
cask "qlstephen"
cask "qlmarkdown"
cask "quicklook-json"
cask "qlprettypatch"
cask "quicklook-csv"
cask "betterzip"
cask "webpquicklook"
cask "suspicious-packa"


### 效率工具
brew "zsh"

# TL;DR 即Too Long; Didn’t Read. 太长不看
brew "tldr"

# https://github.com/cheat/cheat
brew "cheat"

brew "maven"
brew "iproute2mac"
brew "jq"
brew "thefuck"
brew "tig"
brew "fzf"
brew "wget"
brew "zsh-autosuggestions"
brew "ffmpeg"
brew "youtube-dl"

# mas - 一个简单的命令行界面的苹果应用商店
# https://github.com/mas-cli/mas
brew "mas"

# dmg to iso
brew "dmg2img"

# m-cli - 用于 macOS 的瑞士军刀
# https://github.com/rgcr/m-cli
brew "m-cli"

# 解压缩程序
# 压缩的时候可能导致部分信息丢失
cask "the-unarchiver"
#cask "keka"
# 强烈不推荐（遇到解压文件部分丢失信息的情况下，直接终端解压，导致后面的部分数据完全无法解压出来）
#cask "ezip"

# PDF阅读器
cask "skim"

# Mac App Store
mas "Disk Speed Test", id: 425264550
mas "Dr. Cleaner", id: 921458519
mas "看图-轻松找照片", id: 1314842898
mas "Simplenote", id: 692867256
mas "Keynote 讲演", id: 409183694
mas "Numbers 表格", id: 409203825
mas "Pages 文稿", id: 409201541
# mas "Xcode", id: 497799835



# <----------------------------------只是下载安装包，不必通过Homebrew安装的软件--------------------------------->

# 坚果云
cask "nutstore"


# <----------------------------------下载速度太慢，不适合通过Homebrew安装的软件--------------------------------->

cask "intellij-idea-ce"



# <----------------------------------第二批次下载软件--------------------------------->

# 福晰PDF阅读器
cask "foxitreader"

# 切换字体为14pt Source Code Pro Lite
cask "font-source-code-pro"

#Reggy - 正则表达式编辑器
cask "reggy"

# Licecap - 是一款屏幕录制工具输出 GIF，录制过程中可以随意改变录屏范围
# https://www.cockos.com/licecap/
cask "licecap"

# Shifty - 一个菜单栏应用程序，让您更好的控制夜览模式
# https://shifty.natethompson.io/zh-cn/
cask "shifty"

# Kap - 轻量 GIF 录屏小工具
# https://getkap.co/
cask "kap"

# KeyCastr - 录屏好帮手，实时显示按键操作的小工具
# https://github.com/keycastr/keycastr
cask "keycastr"

# 高性能的视频编码和转换工具，具有很好的图形用户界面
cask "handbrake"

# 免费开源视频编辑器
cask "shotcut"

# MediaInfo是一款非常实用的视频音频参数检测工具
cask "mediainfo"

# 参考链接:
# https://github.com/AdoptOpenJDK/homebrew-openjdk
# 安装Java8
# cask "adoptopenjdk8"
cask "java8"
# 安装Java11
#cask "adoptopenjdk11"
# 安装Java8
#cask "homebrew/cask-versions/adoptopenjdk8"

# Dash - 强大到你无法想象的 API 离线文档软件
# https://kapeli.com/dash
cask "dash"

# 可视化的文件比较（也可进行目录比较）与合并工具
# http://sourcegear.com/diffmerge/
cask "diffmerge"

# 删除APP的同时移除文件
# http://onnati.net/apptrap/
cask "apptrap"

# 状态栏的系统监视器
# https://github.com/iglance/iGlance
cask "iglance"

# 隐藏系统菜单栏
# https://matthewpalmer.net/vanilla/
cask "vanilla"

# 磁盘空间使用扫描工具
cask "omnidisksweeper"

# 功能强大的自动化工具，Lua 脚本驱动，支持窗口管理
# https://github.com/S1ngS1ng/HammerSpoon/blob/master/README-cn.md
cask "hammerspoon"

# SourceTree - 强大的 Git 跨平台客户端
# https://www.sourcetreeapp.com/
cask "sourcetree"

# Fork - 一个快速友好的 Git 客户端
# https://git-fork.com/
cask "fork"

# Git图形管理界面
cask "sublime-merge"

# 一个开源的Python发行版本，其包含了conda、Python等180多个科学包及其依赖项
cask "anaconda"

brew "jesseduffield/lazydocker/lazydocker"

brew "lazygit"

# KeepingYouAwake - 替代caffeine，更好地支持Mac中的暗模式,防止电脑进入睡眠
# https://github.com/newmarcel/KeepingYouAwake
cask "keepingyouawake"

# install node.js
brew "node"

# Ag (The Silver Searcher)和 Ack 都是CLI的全局搜索工具，其中Ag更快一些，而Ack也比Vim自带的grep快很多
brew "the_silver_searcher"


# <----------------------------------涉及到手动输入的软件--------------------------------->

# LuLu - 免费的macOS防火墙，旨在阻止未经授权（传出）的网络流量
# https://objective-see.com/products/lulu.html
# cask "lulu"




# <----------------------------------需手动下载安装的软件--------------------------------->

# Mac-CLI - 自动化您的 OS X 系统的使用
# sh -c "$(curl -fsSL https://raw.githubusercontent.com/guarinogabriel/mac-cli/master/mac-cli/tools/install)"
# https://github.com/guarinogabriel/Mac-CLI

# spaceship - 一个简约，功能强大且极易定制的Zsh提示主题
# git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt"
# ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"
# Set ZSH_THEME="spaceship" in your .zshrc
# https://github.com/denysdovhan/spaceship-prompt




# <----------------------------------可以尝试的软件--------------------------------->

# PPRows - 计算你写了多少行代码
# https://github.com/jkpang/PPRows/releases

# iStats 是一个可以让你快速查看电脑 CPU 温度，磁盘转速和电池等信息的命令行工具
# https://github.com/Chris911/iStats

# LNav - 日志文件阅读器
# http://lnav.org/

# What's Your Sign? - 验证文件的加密签名可以推断其来源或可信度
# https://objective-see.com/products/whatsyoursign.html

# Encrypto - 免费加密工具，用于加密文件和文件夹
# https://macpaw.com/encrypto

# OverSight - 监控 Mac 的麦克风和网络摄像头，当内部麦克风被激活，或者当进程访问摄像头时提醒用户
# https://objective-see.com/products/oversight.html

# TaskExplorer - 使用 TaskExplorer 探索在 Mac 上运行的所有任务（进程）
# https://objective-see.com/products/taskexplorer.html

# Cleaner for Xcode - Xcode 的清理工具，清理几十G应该不是问题
# https://github.com/waylybaye/XcodeCleaner




# <----------------------------------收费的软件--------------------------------->

# Mac OS 电脑硬件信息检测软件
# cask "istat-menus"

# Beyond Compare - 对比两个文件夹或者文件，并将差异以颜色标示
# http://www.scootersoftware.com/download.php

# QSpace - 一款简洁高效的多视图文件管理器
# https://qspace.awehunt.com/




# <----------------------------------暂时不下载的软件--------------------------------->

# BetterRename - 一款强大的批量重命名工具，可以通过搜索功能改名
# https://www.publicspace.net/BetterRename/

# Emacs - Emacs 是基于控制台的编辑器和高度可定制的
# https://www.emacswiki.org/emacs/EmacsForMacOS

# TextMate - 文本编辑器软件，与 BBedit 一起并称苹果机上的 emacs 和 vim
# https://macromates.com/

# BBEdit - 强大的文件编辑器，用于编辑文件，文本文件及程序源代码
# http://www.barebones.com/products/bbedit/

# Android Studio - Android 的官方 IDE，基于 Intellij IDEA
# https://developer.android.com/studio/index.html

# SnippetsLab - 管理和组织你的代码片段。
# https://www.renfei.org/snippets-lab/

# LaunchRocket - 在 Mac 系统偏好设置中创建服务管理
#https://github.com/jimbojsb/launchrocket

# Cocoa Rest Client - 比 Postman 看起来漂亮的客户端，测试 HTTP/REST endpoints
# https://mmattozzi.github.io/cocoa-rest-client/

# Wireshark - 世界上最广泛使用的网络协议分析软件
# https://www.wireshark.org/

# Charles - 一个代理工具，允许你查看所有的 HTTP 和 HTTPS 流量
# https://www.charlesproxy.com/

# James - 用于 https 和 http 进行查询映射请求
# https://github.com/james-proxy/james

# mitmproxy - 一款支持 HTTP(S) 的中间人代理工具，可在终端下运行，可用于抓包
# https://mitmproxy.org/

# Proxyman - 适用于 macOS 的现代直观 HTTP 调试代理
# https://proxyman.io/

# Glances - 在命令行中查看你系统运行状态的工具
# https://github.com/nicolargo/glances

# httpie - HTTPie 是一个让你微笑的命令行 HTTP 客户端
# https://httpie.org/

# ndm - 查看本地NPM安装的包客户端软件。摆脱命令方便安装、查看、卸载软件
# https://720kb.github.io/ndm/

# silver searcher (ag) - 类似于ack的代码搜索工具，专注于速度
# https://github.com/ggreer/the_silver_searcher

# job - 短命令并发、重复执行工具, 适用于压测
# https://github.com/liujianping/job

# ttygif - 将终端录制转换为 GIF 动画
# https://github.com/icholy/ttygif

# GIMP - 图像编辑软件，号称 Linux 下的 PhotoShop，同时有 Mac 版本
# https://www.gimp.org/

# CheatSheet - CheatSheet 是一款 Mac 上的非常实用的快捷键快速提醒工具
# https://www.mediaatelier.com/CheatSheet/

# Keytty - 让你通过键盘使用鼠标
# http://keytty.com/

# 控制Mac独立显卡与集成显卡之间的切换
# https://gfx.io/
# cask "gfxcardstatus"

# TeamViewer - 远程协助及在线协作和会议功能的软件，商业软件个人使用免费
# https://www.teamviewer.com/
# cask "teamviewer"
