# centos8设置静态IP
```sh
cd /etc/sysconfig/network-scripts/    # 进入网络配置文件夹
vi ifcfg-enp0s3                                 # 编辑配置文件
```

```sh
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=enp0s3
UUID=9dfefcc8-3494-47dc-85ae-44b322710d1e
DEVICE=enp0s3
ONBOOT=no
```

添加相关ip和网关等配置。

```sh
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static				# 设置为static为静态
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=enp0s3
UUID=9dfefcc8-3494-47dc-85ae-44b322710d1e
DEVICE=enp0s3
ONBOOT=yes				# 设置开机启动设置
IPADDR=192.168.31.220      # 该虚拟机的静态IP，这边要跟宿主机的保持同一网段
GATEWAY=192.168.31.1    # 网关地址
NETMASK=255.255.255.0    # 子网掩码
DNS1=119.29.29.29    # DNS服务器
```

`centos7`下使用`systemctl restart network`进行重启网络服务。而`centos8`需要使用`systemctl restart NetworkManager` 进行重启网络。

# 参考链接

[centos8设置静态IP_lxw的博客-CSDN博客_centos8配置静态ip](https://blog.csdn.net/lxw983520/article/details/102877687)
